package com.visualization.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RobotBeacon implements VisualizationBeaconSignal {
    long localTimestamp;
    String robotName;
    //todo change to team name enum!
    String teamName;
    String task;
    Integer robotId;
}
