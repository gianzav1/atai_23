package com.robot_communication.services;

import com.rcll.protobuf_lib.ProtobufServer;
import com.rcll.protobuf_lib.RobotConnections;
import com.rcll.protobuf_lib.RobotMessageRegister;
import com.rcll.robot.RobotClient;
import com.rcll.robot.RobotTaskCreator;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.*;


@CommonsLog
public class GripsRobotClient extends RobotClient {
    public GripsRobotClient(RobotTaskCreator robotTaskCreator, RobotConnections robotConnections) {
        super(robotTaskCreator, robotConnections);
        RobotMessageRegister.getInstance().add_message(GripsMidlevelTasksProtos.GripsMidlevelTasks.class);
        RobotMessageRegister.getInstance().add_message(GripsPrepareMachineProtos.GripsPrepareMachine.class);
        RobotMessageRegister.getInstance().add_message(GripsExplorationReportMachineProtos.GripsExplorationReportMachine.class);
        RobotMessageRegister.getInstance().add_message(GripsExplorationFoundMachinesProtos.GripsExplorationFoundMachines.class);
        RobotMessageRegister.getInstance().add_message(GripsStartPositionsProtos.GripsStartPositions.class);
        RobotMessageRegister.getInstance().add_message(GripsPrepareMachineProtos.GripsMachineReadyAtOutput.class);
    }

    public void sendGripsTaskToRobot(GripsTasksProtos.GripsTask task) {
        log.info("Sending Task: " + task.getTaskId() + " to robot: " + task.getRobotId() + " - " + task.toString());
        try {
            robotConnections.getRobot(task.getRobotId()).setTimeLastTaskAssignment(System.currentTimeMillis());
            this.sendToRobot(task.getRobotId(), task);
        } catch (Exception e) {
            log.warn("robot not found, retrying after 0.3 seconds");
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                throw new RuntimeException(ex);
            }
            sendGripsTaskToRobot(task);
        }
    }
}

