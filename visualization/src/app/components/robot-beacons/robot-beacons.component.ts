import {Component, Input} from '@angular/core';
import {RobotBeacon} from "./robot-beacon/robot-beacon.component";


@Component({
  selector: 'app-robot-beacons',
  templateUrl: './robot-beacons.component.html',
  styleUrls: ['./robot-beacons.component.scss']
})
export class RobotBeaconsComponent {
  @Input() robot1!: RobotBeacon
  @Input() robot2!: RobotBeacon
  @Input() robot3!: RobotBeacon}
