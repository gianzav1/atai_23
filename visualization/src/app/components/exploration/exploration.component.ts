import {Component, Input} from '@angular/core';

interface IObservation {
  robotId: number;
  machineName: any;
  count: number;
  machineZone: string;
  orientation: number;
}

@Component({
  selector: 'app-exploration',
  templateUrl: './exploration.component.html',
  styleUrls: ['./exploration.component.scss']
})
export class ExplorationComponent {
  @Input()
  public data: IObservation[] = [];
  displayedColumns: string[] = ["robotId", "count", "name", "machineZone", "orientation"];
}
