package com.grips.tools;

import com.grips.config.GameFieldConfig;
import com.shared.domain.Point2d;
import com.shared.domain.Point2dInt;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PathEstimatorTest {

    //@Test
    void rcllToGrid1() {
        Point2dInt grid = test(5, 5, 1.5, 2.5);
        assertEquals(grid.getX(), 65, 25);
    }

    //@Test
    void rcllToGrid2() {
        Point2dInt grid = test(5, 5, -1.5, 2.5);
        assertEquals(grid.getX(), 35, 25);
    }

    private Point2dInt test(int height, int width, double x, double y) {
        PathEstimator pathEstimator = new PathEstimator(null, new GameFieldConfig(height, width, ""));
        return pathEstimator.rcllToGrid(new Point2d(x, y));
    }
}