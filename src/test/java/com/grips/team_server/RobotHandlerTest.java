/*
 *
 * Copyright (c) 2017, Graz Robust and Intelligent Production System (grips)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.grips.team_server;

import com.grips.tools.DiscretizeAngles;
import org.junit.jupiter.api.Test;
import org.robocup_logistics.llsf_msgs.MachineInstructionProtos;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by grips on 01.07.17.
 */
class RobotHandlerTest {
    @Test
    void discretizeAngles() {
        assertEquals(DiscretizeAngles.discretizeAngles(0), 0);
        assertEquals(DiscretizeAngles.discretizeAngles(10), 0);
        assertEquals(DiscretizeAngles.discretizeAngles(22), 0);
        assertEquals(DiscretizeAngles.discretizeAngles(23), 45);
        assertEquals(DiscretizeAngles.discretizeAngles(30), 45);
        assertEquals(DiscretizeAngles.discretizeAngles(45), 45);
        assertEquals(DiscretizeAngles.discretizeAngles(50), 45);
        assertEquals(DiscretizeAngles.discretizeAngles(67), 45);
        assertEquals(DiscretizeAngles.discretizeAngles(68), 90);
        assertEquals(DiscretizeAngles.discretizeAngles(275), 270);
        assertEquals(DiscretizeAngles.discretizeAngles(300), 315);
        assertEquals(DiscretizeAngles.discretizeAngles(337), 315);
        assertEquals(DiscretizeAngles.discretizeAngles(338), 0);
        assertEquals(DiscretizeAngles.discretizeAngles(359), 0);
    }

    @Test
    public void test1() {
        org.robocup_logistics.llsf_msgs.MachineInstructionProtos.PrepareInstructionDS asdf =
                org.robocup_logistics.llsf_msgs.MachineInstructionProtos.PrepareInstructionDS.newBuilder()
                .setOrderId(123)
                .build();
        MachineInstructionProtos.PrepareInstructionDS tmp = MachineInstructionProtos.PrepareInstructionDS
                .newBuilder()
                .setOrderId(123)
                .build();
    }
}
