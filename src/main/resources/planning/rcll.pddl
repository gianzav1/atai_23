(define (domain rcll)
 (:requirements :strips :typing :durative-actions :fluents )
 (:types base_station cap_station_input cap_station_output delivery_station starting ring_station_input ring_station_output - location
   location holding_station - station
   cap_station ring_station - holding_station
   robot product ccolor rcolor numbers)

  (:predicates 
     (at ?r - robot ?l - location)
	(holdafter ?p - product)
     (free ?r - robot)
     (empty ?l - location)
     (holding ?r - robot ?p - product)
     (n_holding ?r - robot)
     (holding_base ?r - robot)
     (holding_capbase ?r - robot)
     (station_holding ?cs - holding_station ?p - product)
     (n_station_holding ?cs - holding_station)
     (cs_station_has_capbase ?cs - cap_station)
     (n_cs_station_has_capbase ?cs - cap_station)
     (parent_cs ?l - location ?cs - cap_station)
     (parent_rs ?l - location ?cs - ring_station)
	 (stage_c0_0 ?p - product)
     (stage_c0_1 ?p - product)
     (stage_c0_2 ?p - product)
     (stage_c0_3 ?p - product)
     (stage_c0_4 ?p - product)
	 (todeliver ?p - product)
	 (delivered ?p - product)
	 (capcolor ?p - product ?c - ccolor)
	 (stationcapcolor ?cs - cap_station ?c - ccolor)
	 (ring1color ?p - product ?c - rcolor)
	 (ring2color ?p - product ?c - rcolor)
	 (ring3color ?p - product ?c - rcolor)
	 (stationringcolor ?rs - ring_station ?c - rcolor)
	 (basetodispose ?cs - cap_station)
	 (n_basetodispose ?cs - cap_station)
	(tocheck ?p - product)
	(complexity0 ?p - product)
	(complexity1 ?p - product)
	(complexity2 ?p - product)
	(complexity3 ?p - product)
	(loadedbases ?rs - ring_station ?x - numbers)
	(requiredbases ?c - rcolor  ?x - numbers)
	(increasebyone ?x - numbers ?res - numbers)
	(subtract ?x1 - numbers ?x2 - numbers ?res - numbers)

 )

 (:functions
    (distance ?l1 ?l2 - location)
	(distanceBuffer ?l1 ?l2 - location)
	(deliveryWindowStart ?p - product)
 )


(:durative-action waitForDeliveryWindow 
	:parameters (?p - product)
	:duration (= ?duration (deliveryWindowStart ?p) )
	:condition (and (at start (tocheck ?p)))
	:effect (and
		(at end (holdafter ?p))
		(at end (not (tocheck ?p)))
 	)
)



(:durative-action move_getBaseFromBScriticalTask 
	:parameters (?r - robot ?bs - base_station ?p - product ?l - location)
	:duration (= ?duration  (distance ?l ?bs)  ) 
	:condition (and 
		(at start (at ?r ?l))
		(at start (empty ?bs))
		(at start (free ?r))
		(at start (stage_c0_0 ?p))
		(at start (n_holding ?r))
	)
	:effect (and
		(at start (at ?r ?bs))
		(at start (not (empty ?bs)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (stage_c0_1 ?p ))
		(at end (not (stage_c0_0 ?p)))
		(at end (holding ?r ?p))
		(at end (not (n_holding ?r)))

 	)
)



(:durative-action move_deliverProductC0ToCScriticalTask 
	:parameters (?r - robot ?csi - cap_station_input ?cs - cap_station ?p - product ?l - location ?c - ccolor)
	:duration (= ?duration   (distance ?l ?csi)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (empty ?csi))
		(at start (free ?r))
		(at start (parent_cs ?csi ?cs))
		(at start (holding ?r ?p))
		(at start (stage_c0_1 ?p))
		(at start (complexity0 ?p))
		(at start (n_basetodispose ?cs))
		
		(at start (n_station_holding ?cs))
		(at start (cs_station_has_capbase ?cs))		

		(at start (capcolor ?p ?c))
		(at start (stationcapcolor ?cs ?c))
	)
	:effect (and
		(at start (not (empty ?csi)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?csi))
		(at start (not (free ?r)))
		(at start (not (stage_c0_1 ?p)))
		(at end (free ?r))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?cs ?p))
		(at end (not (n_station_holding ?cs)))
	)
)

(:durative-action move_deliverProductC1ToCScriticalTask 
	:parameters (?r - robot ?csi - cap_station_input ?cs - cap_station ?p - product ?l - location ?c - ccolor)
	:duration (= ?duration   (distance ?l ?csi)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (empty ?csi))
		(at start (free ?r))
		(at start (parent_cs ?csi ?cs))
		(at start (holding ?r ?p))
		(at start (stage_c0_2 ?p))
		(at start (complexity1 ?p))
		(at start (n_basetodispose ?cs))
		
		(at start (n_station_holding ?cs))
		(at start (cs_station_has_capbase ?cs))		

		(over all (capcolor ?p ?c))
		(over all (stationcapcolor ?cs ?c))
	)
	:effect (and
		(at start (not (empty ?csi)))
		(at start (empty ?l))
		(at start (not (stage_c0_2 ?p)))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?csi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?cs ?p))
		(at end (not (n_station_holding ?cs)))
	)
)

(:durative-action move_deliverProductC2ToCScriticalTask 
	:parameters (?r - robot ?csi - cap_station_input ?cs - cap_station ?p - product ?l - location ?c - ccolor)
	:duration (= ?duration   (distance ?l ?csi)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (empty ?csi))
		(at start (free ?r))
		(at start (parent_cs ?csi ?cs))
		(at start (holding ?r ?p))
		(at start (stage_c0_3 ?p))
		(at start (complexity2 ?p))
		(at start (n_basetodispose ?cs))
		
		(at start (n_station_holding ?cs))
		(at start (cs_station_has_capbase ?cs))		

		(over all (capcolor ?p ?c))
		(over all (stationcapcolor ?cs ?c))
	)
	:effect (and
		(at start (not (empty ?csi)))
		(at start (empty ?l))
		(at start (not (stage_c0_3 ?p)))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?csi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?cs ?p))
		(at end (not (n_station_holding ?cs)))

	)
)

(:durative-action move_deliverProductC3ToCScriticalTask 
	:parameters (?r - robot ?csi - cap_station_input ?cs - cap_station ?p - product ?l - location ?c - ccolor)
	:duration (= ?duration   (distance ?l ?csi)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (empty ?csi))
		(at start (free ?r))
		(at start (parent_cs ?csi ?cs))
		(at start (holding ?r ?p))
		(at start (stage_c0_4 ?p))
		(at start (complexity3 ?p))
		(at start (n_basetodispose ?cs))
		
		(at start (n_station_holding ?cs))
		(at start (cs_station_has_capbase ?cs))		

		(over all (capcolor ?p ?c))
		(over all (stationcapcolor ?cs ?c))
	)
	:effect (and
		(at start (not (empty ?csi)))
		(at start (not (stage_c0_4 ?p)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?csi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?cs ?p))
		(at end (not (n_station_holding ?cs)))
	)
)

(:durative-action move_getCapBaseFromCSresourceTask 
	:parameters (?r - robot ?csi - cap_station_input ?cs - cap_station ?l - location)
	:duration (= ?duration   (distanceBuffer ?l ?csi)  ) 
	:condition (and 
		(at start (at ?r ?l))
		(at start (empty ?csi))			 
		(at start (free ?r))
		(at start (parent_cs ?csi ?cs))
		(at start (n_holding ?r))
		(at start (n_cs_station_has_capbase ?cs))
		(at start (n_station_holding ?cs))
	)
	:effect (and
		(at start (not (empty ?csi)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?csi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (cs_station_has_capbase ?cs))
		(at end (not (n_cs_station_has_capbase ?cs)))
		(at end (basetodispose ?cs))
		(at end (not (n_basetodispose ?cs)))
	)
)



(:durative-action move_getProductFromCScriticalTask 
	:parameters (?r - robot ?cso - cap_station_output ?cs - cap_station ?p - product ?l - location)
	:duration (= ?duration   (distance ?l ?cso)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (empty ?cso))
		(at start (free ?r))
		(at start (parent_cs ?cso ?cs))
		(at start (n_holding ?r))
		(at start (station_holding ?cs ?p))
	)
	:effect (and
		(at start (not (empty ?cso)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?cso))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (holding ?r ?p))
		(at end (not (n_holding ?r)))
		(at end (todeliver ?p ))
		;(at end (not (stage_c0_1 ?p)))
		(at end (not (station_holding ?cs ?p)))
		(at end (n_station_holding ?cs))
        (at end (n_cs_station_has_capbase ?cs))
        (at start (not (cs_station_has_capbase ?cs)))
	)
)


(:durative-action move_deliverProductToRS1criticalTask
	:parameters (?r - robot ?rs - ring_station ?rsi - ring_station_input ?p - product ?l - location ?c - rcolor ?x1 - numbers ?x2 - numbers ?res - numbers)
	:duration (= ?duration   (distance ?l ?rsi)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (free ?r)) 
		(at start (empty ?rsi))
		(at start (stage_c0_1 ?p))
		(at start (holding ?r ?p))
		;(at start (>= (loadedbases ?rs) (requiredbases ?c) ) )
		(at start (loadedbases ?rs ?x1))
		(at start (requiredbases ?c ?x2))
		(at start (subtract ?x1 ?x2 ?res))

		(over all (ring1color ?p ?c)) 
		(over all (stationringcolor ?rs ?c))
		(at start (parent_rs ?rsi ?rs))
		(at start (n_station_holding ?rs))
	)
	:effect (and 
		;(at end (decrease (loadedbases ?rs) (requiredbases ?c)))
		(at end (not (loadedbases ?rs ?x1)))
		(at end (loadedbases ?rs ?res))

		(at start (not (empty ?rsi)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?rsi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (not (stage_c0_1 ?p)))	
		(at end (stage_c0_2 ?p))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?rs ?p))
		(at end (not (n_station_holding ?rs)))
	)
)



(:durative-action move_deliverProductToRS2criticalTask
	:parameters (?r - robot ?rs - ring_station ?rsi - ring_station_input ?p - product ?l - location ?c - rcolor ?x1 - numbers ?x2 - numbers ?res - numbers)
	:duration (= ?duration   (distance ?l ?rsi)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (free ?r)) 
		(at start (empty ?rsi))
		(at start (stage_c0_2 ?p))
		(at start (holding ?r ?p))
		;(at start (>= (loadedbases ?rs) (requiredbases ?c) ) )
		(at start (loadedbases ?rs ?x1))
		(at start (requiredbases ?c ?x2))
		(at start (subtract ?x1 ?x2 ?res))

		(over all (ring2color ?p ?c)) 
		(over all (stationringcolor ?rs ?c))
		(at start (parent_rs ?rsi ?rs))
		(at start (n_station_holding ?rs))
	)
	:effect (and 
		;(at end (decrease (loadedbases ?rs) (requiredbases ?c)))
		(at end (not (loadedbases ?rs ?x1)))
		(at end (loadedbases ?rs ?res))

		(at start (not (empty ?rsi)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?rsi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (not (stage_c0_2 ?p)))	
		(at end (stage_c0_3 ?p))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?rs ?p))
		(at end (not (n_station_holding ?rs)))
	)
)

(:durative-action move_deliverProductToRS3criticalTask
	:parameters (?r - robot ?rs - ring_station ?rsi - ring_station_input ?p - product ?l - location ?c - rcolor ?x1 - numbers ?x2 - numbers ?res - numbers)
	:duration (= ?duration   (distance ?l ?rsi)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (free ?r)) 
		(at start (empty ?rsi))
		(at start (stage_c0_3 ?p))
		(at start (holding ?r ?p))
		;(at start (>= (loadedbases ?rs) (requiredbases ?c) ) )
		(at start (loadedbases ?rs ?x1))
		(at start (requiredbases ?c ?x2))
		(at start (subtract ?x1 ?x2 ?res))

		(over all (ring3color ?p ?c)) 
		(over all (stationringcolor ?rs ?c))
		(at start (parent_rs ?rsi ?rs))
		(at start (n_station_holding ?rs))
	)
	:effect (and 
		;(at end (decrease (loadedbases ?rs) (requiredbases ?c)))
		(at end (not (loadedbases ?rs ?x1)))
		(at end (loadedbases ?rs ?res))

		(at start (not (empty ?rsi)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?rsi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (not (stage_c0_3 ?p)))	
		(at end (stage_c0_4 ?p))
		(at end (not (holding ?r ?p)))
		(at end (n_holding ?r))
		(at end (station_holding ?rs ?p))
		(at end (not (n_station_holding ?rs)))
	)
)

(:durative-action move_getProductFromRSCriticalTask 
	:parameters (?r - robot ?rs - ring_station ?rso - ring_station_output ?p - product ?l - location)
	:duration (= ?duration   (distance ?l ?rso)  ) 
	:condition (and 
		(at start (at ?r ?l))
		(at start (empty ?rso))
		(at start (free ?r))
		(at start (n_holding ?r))
		(at start (station_holding ?rs ?p))
		(at start (parent_rs ?rso ?rs))
	)
	:effect (and
		(at start (at ?r ?rso))
		(at start (not (empty ?rso)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (holding ?r ?p))
		(at end (not (n_holding ?r)))
		(at end (not (station_holding ?rs ?p)))
        (at end (n_station_holding ?rs))
 	)
)

(:durative-action move_getBaseFromBSResourceTask 
	:parameters (?r - robot ?bs - base_station ?l - location)
	:duration (= ?duration   (distance ?l ?bs)  ) 
	:condition (and 
		(at start (at ?r ?l))
		(at start (empty ?bs))
		(at start (free ?r))
		(at start (n_holding ?r))
	)
	:effect (and
		(at start (at ?r ?bs))
		(at start (not (empty ?bs)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (holding_base ?r))
		(at end (not (n_holding ?r)))

 	)
)

(:durative-action move_getBaseFromCSResourceTask 
	:parameters (?r - robot ?cso - cap_station_output ?cs - cap_station ?l - location)
	:duration (= ?duration   (distance ?l ?cso)  ) 
	:condition (and 
		(at start (at ?r ?l))
		(at start (empty ?cso))
		(at start (free ?r))
		(at start (parent_cs ?cso ?cs))
		(at start (n_holding ?r))
		(at start (basetodispose ?cs))
	)
	:effect (and
		(at start (at ?r ?cso))
		(at start (not (empty ?cso)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (holding_base ?r))
		(at end (not (n_holding ?r)))
		(at end (not (basetodispose ?cs)))
		(at end (n_basetodispose ?cs))

 	)
)

(:durative-action move_deliverBaseToRSResourceTask
	:parameters (?r - robot ?rs - ring_station ?rsi - ring_station_input ?l - location ?x1  - numbers ?res - numbers)
	:duration (= ?duration   (distance ?l ?rsi)  ) 
	:condition (and 
		;(at start (> 3 (loadedbases ?rs )))
		(at start (loadedbases ?rs ?x1))
		
		(at start (at ?r ?l))
		(at start (empty ?rsi))
		(at start (free ?r))
		(at start (holding_base ?r))
		(at start (parent_rs ?rsi ?rs))

		(at start (increasebyone ?x1 ?res))
	)
	:effect (and 
		(at start (not (empty ?rsi)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?rsi))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (not (holding_base ?r)))
		(at end (n_holding ?r))
		
		;(at end (increase (loadedbases ?rs) 1))
		
		(at end (loadedbases ?rs ?res))
		(at end (not (loadedbases ?rs ?x1)))
	)
)


(:durative-action move_deliverProductToDScriticalTask
	:parameters (?r - robot ?ds - delivery_station ?p - product ?l - location)
	:duration (= ?duration   (distance ?l ?ds)  ) 
	:condition (and  
		(at start (at ?r ?l))
		(at start (empty ?ds))
		(at start (free ?r))
		(at start (holding ?r ?p))	
		(at start (todeliver ?p))
		(at start (holdafter ?p))
	)
	:effect (and
		(at start (not (empty ?ds)))
		(at start (empty ?l))
		(at start (not (at ?r ?l)))
		(at start (at ?r ?ds))
		(at start (not (free ?r)))
		(at end (free ?r))
		(at end (n_holding ?r))
		(at end (not (holding ?r ?p)))
		(at end (delivered ?p ))
		(at end (not (todeliver ?p)))
	)
)






)
