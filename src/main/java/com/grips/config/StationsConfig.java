package com.grips.config;


import com.rcll.domain.Machine;
import com.rcll.domain.MachineName;
import com.rcll.domain.TeamColor;
import com.rcll.refbox.TeamConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "stations")
@Getter
@Setter
public class StationsConfig {
    private boolean useCs1;
    private boolean useCs2;
    private boolean useRs1;
    private boolean useRs2;
    private boolean useDs;
    private boolean useSs;
    private boolean useBs;

    public Integer countUsedMachines() {
        Integer numberMachines = 0;
        if (useCs1) numberMachines++;
        if (useCs2) numberMachines++;
        if (useRs1) numberMachines++;
        if (useRs2) numberMachines++;
        if (useDs) numberMachines++;
        if (useSs) numberMachines++;
        if (useBs) numberMachines++;
        return numberMachines;
    }

    public List<MachineName> getUsedStations(TeamColor teamColor) {
        List<MachineName> machineNames = new ArrayList<>();
        if (useCs1) {
            machineNames.add(new MachineName(teamColor, Machine.CS1));
        }
        if (useCs2) {
            machineNames.add(new MachineName(teamColor, Machine.CS2));
        }
        if (useRs1) {
            machineNames.add(new MachineName(teamColor, Machine.RS1));
        }
        if (useRs2) {
            machineNames.add(new MachineName(teamColor, Machine.RS2));
        }
        if (useBs) {
            machineNames.add(new MachineName(teamColor, Machine.BS));
        }
        if (useDs) {
            machineNames.add(new MachineName(teamColor, Machine.DS));
        }
        if (useSs) {
            machineNames.add(new MachineName(teamColor, Machine.SS));
        }
        return machineNames;
    }
}
