package com.grips.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "teamcolors")
@Getter
@Setter

public class TeamColor {
    private RobotPose robot1;
    private RobotPose robot2;
    private RobotPose robot3;
}