package com.grips.monitoring;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetProductTask {
    private Long robotId;
    private String machine;
    private String machinePoint;
}
