package com.grips.robot.msg_handler;

import com.grips.scheduler.GameField;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.rcll.domain.MachineName;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class PrsTaskMsgHandler implements Consumer<AgentTasksProtos.AgentTask> {

    private final GameField gameField;
    private final ExplorationScheduler explorationScheduler;
    private final IScheduler productionScheduler;

    public PrsTaskMsgHandler(GameField gameField,
                             ExplorationScheduler explorationScheduler,
                             @Qualifier("production-scheduler") IScheduler productionScheduler) {
        this.gameField = gameField;
        this.explorationScheduler = explorationScheduler;
        this.productionScheduler = productionScheduler;}

    @Override
    public void accept(AgentTasksProtos.AgentTask prsTask) {
        if (!prsTask.hasSuccessful()) {
            log.warn("Successfull flag is not set on Task, not knowing if success or failure!");
        }
        if (prsTask.hasExploreMachine()) {
            //System.out.println("Received PrsTask with exploreMachineAtWaypointTask.");
            log.info("Received exploreMachineAtWaypointTaskResult from Robot " + prsTask.getRobotId() + " Zone was: " +
                    prsTask.getExploreMachine().getWaypoint() + " Machine was: " + prsTask.getExploreMachine().getMachineId());
            String zoneName = prsTask.getExploreMachine().getWaypoint();
            // Update Zone with exploration state
            if (prsTask.getSuccessful()) {
                log.info("Successfully explored zone " + prsTask.getExploreMachine().getWaypoint());
                explorationScheduler.explorationTaskSucess(prsTask);
            } else {
                log.info("Exploration at zone " + prsTask.getExploreMachine().getWaypoint() + " for machine " + prsTask.getExploreMachine().getMachineId() + " failed!");
                explorationScheduler.explorationTaskFailed(prsTask.getRobotId(), new MachineName(prsTask.getExploreMachine().getMachineId()), prsTask.getExploreMachine().getWaypoint());
            }
        } else if (prsTask.hasDeliver() || prsTask.hasRetrieve() || prsTask.hasMove() || prsTask.hasBuffer()) {
            if (!productionScheduler.handleRobotTaskResult(prsTask)) {
                explorationScheduler.handleRobotTaskResult(prsTask);
            }
        } else {
            log.error("Unknown task result received: " + prsTask.toString());
        }

    }
}
