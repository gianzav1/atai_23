package com.grips.robot.msg_handler;

import com.grips.persistence.domain.MachineReport;
import com.grips.persistence.dao.MachineReportDao;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.grips.tools.DiscretizeAngles;
import lombok.extern.apachecommons.CommonsLog;
import org.dozer.Mapper;
import org.robocup_logistics.llsf_msgs.GripsMachineHandlingReportMachineProtos;
import org.robocup_logistics.llsf_msgs.MachineReportProtos;
import org.robocup_logistics.llsf_msgs.ZoneProtos;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
@CommonsLog
public class MachineOrientationMsgHandler implements Consumer<GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine> {

    private final MachineReportDao machineReportDao;
    private final Mapper mapper;
    private final ExplorationScheduler explorationScheduler;

    public MachineOrientationMsgHandler(MachineReportDao machineReportDao,
                                        Mapper mapper,
                                        ExplorationScheduler explorationScheduler) {
        this.machineReportDao = machineReportDao;
        this.mapper = mapper;
        this.explorationScheduler = explorationScheduler;
    }

    @Override
    public void accept(GripsMachineHandlingReportMachineProtos.GripsMachineHandlingReportMachine machineOrientation) {
        //System.out.println("orientation received: " + machineOrientation.getOrientation());
        log.info("MachineOrientaitonMsgHandler is used!!!");
        //int rotation = (int)(machineOrientation.getOrientation()*180.0/Math.PI) + 180;
        int rotation = (int) (machineOrientation.getOrientation() * 180.0 / Math.PI);
        rotation = (rotation % 360 + 360) % 360;

        rotation = DiscretizeAngles.discretizeAngles(rotation);

        String zone = machineOrientation.getZonePrefix() + "_Z";
        zone += Integer.toString(machineOrientation.getZoneNumber());

        MachineReportProtos.MachineReportEntry entry = MachineReportProtos.MachineReportEntry.newBuilder()
                .setName(machineOrientation.getMachineId())
                .setZone(ZoneProtos.Zone.valueOf(zone))
                .setRotation(rotation).build();

        log.info("Machine Reported: name  = " + machineOrientation.getMachineId() + " orientation = " + rotation);

        MachineReport machineReport = mapper.map(entry, MachineReport.class);
        machineReportDao.save(machineReport);
       // explorationScheduler.explorationTaskResult(machineOrientation.getRobotId(), new MachineName(machineOrientation.getMachineId()), zone);
    }
}
