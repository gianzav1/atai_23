package com.grips.scheduler.api;

import com.rcll.domain.Peer;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

public interface IScheduler {
    void handleBeaconSignal(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot);
    boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask);
    void handleMachineInfo(MachineInfoProtos.Machine machine);
}
