package com.grips.scheduler.api;

import com.grips.persistence.dao.LockPartDao;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@CommonsLog
public class LockPartService {
    private final LockPartDao lockPartDao;

    @Autowired
    public LockPartService(LockPartDao lockPartDao) {
        this.lockPartDao = lockPartDao;
    }

    public int getMaterialCount(String machine) {
        int returner = lockPartDao.countByMachine(machine);
        log.info("getMaterialCount for Machine: : " + machine + " is:" + returner);
        return returner;
    }
}
