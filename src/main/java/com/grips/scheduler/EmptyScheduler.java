package com.grips.scheduler;

import com.grips.scheduler.api.IScheduler;
import com.rcll.domain.Peer;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

@CommonsLog
public class EmptyScheduler implements IScheduler {
    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        log.info("Got Beacon Signal from: " + robotId);
    }

    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
        log.info("Got Task Result: " + prsTask.toString());
        return true;
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {

    }
}
