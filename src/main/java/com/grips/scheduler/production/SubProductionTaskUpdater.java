package com.grips.scheduler.production;

import com.grips.config.RefboxConfig;
import com.grips.scheduler.api.IScheduler;
import com.grips.scheduler.exploration.ExplorationScheduler;
import com.rcll.domain.TeamColor;
import com.rcll.refbox.RefboxClient;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;
import org.robocup_logistics.llsf_msgs.TeamProtos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class SubProductionTaskUpdater {
    @Autowired
    @Qualifier("production-scheduler")
    private IScheduler productionScheduler;

    @Autowired
    private ExplorationScheduler explorationScheduler;

    @Autowired
    private RefboxClient refboxClient;

    public void updateTaskPerMachine(MachineInfoProtos.MachineInfo info) {
        TeamProtos.Team team = refboxClient.getTeamColor().map(x -> TeamColor.CYAN.equals(x) ? TeamProtos.Team.CYAN : TeamProtos.Team.MAGENTA).orElseThrow();
        if (info.getTeamColor().equals(team)) {
            info.getMachinesList().forEach(machine -> {
                productionScheduler.handleMachineInfo(machine);
                explorationScheduler.handleMachineInfo(machine);
            });
        }
    }
}
