package com.grips.scheduler.challanges;

import com.grips.scheduler.api.IScheduler;
import com.rcll.domain.Peer;
import com.robot_communication.services.GripsRobotClient;
import com.robot_communication.services.GripsRobotTaskCreator;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.AgentTasksProtos;
import org.robocup_logistics.llsf_msgs.BeaconSignalProtos;
import org.robocup_logistics.llsf_msgs.MachineInfoProtos;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@CommonsLog
public class GraspingScheduler implements IScheduler {

    private final GripsRobotTaskCreator robotTaskCreator;
    private final GripsRobotClient robotClient;

    private final String robot1Machine = "M-CS1";

    private List<AgentTasksProtos.AgentTask> robot1_task;
    private int robot1_current_index;


    private Map<Long, Boolean> recentlyAssigned;

    public GraspingScheduler(GripsRobotTaskCreator robotTaskCreator, GripsRobotClient robotClient) {
        this.robotTaskCreator = robotTaskCreator;
        this.robotClient = robotClient;
        this.recentlyAssigned = new ConcurrentHashMap<Long, Boolean>();
        this.recentlyAssigned.put(1L, false);
        this.recentlyAssigned.put(2L, false);
        this.recentlyAssigned.put(3L, false);
    }

    @Override
    public void handleBeaconSignal(BeaconSignalProtos.BeaconSignal beacon_signal, int robotId, Peer robot) {
        if (beacon_signal.getTask().getTaskId() == -1) {
            this.recentlyAssigned.put((long) robotId, true);
            robotClient.sendGripsTaskToRobot(robotTaskCreator.createGraspingChallengeNew((long) robotId));
        } else {
            this.recentlyAssigned.put((long) robotId, false);
        }
    }

    @Override
    public boolean handleRobotTaskResult(AgentTasksProtos.AgentTask prsTask) {
        return true;
    }

    @Override
    public void handleMachineInfo(MachineInfoProtos.Machine machine) {

    }
}
