package com.grips.scheduler.asp.dispatcher;


import com.grips.config.RefboxConfig;
import com.grips.persistence.domain.ProductOrder;
import com.grips.persistence.domain.SubProductionTask;
import com.grips.persistence.domain.SubProductionTaskBuilder;
import com.rcll.domain.*;
import com.rcll.planning.encoding.IntOp;
import com.rcll.refbox.RefboxClient;
import lombok.extern.apachecommons.CommonsLog;
import org.robocup_logistics.llsf_msgs.MachineDescriptionProtos;
import org.robocup_logistics.llsf_msgs.ProductColorProtos.BaseColor;
import org.robocup_logistics.llsf_msgs.ProductColorProtos.CapColor;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@CommonsLog
public class ActionMapping {

    private final RefboxClient refboxClient;
    private List<String> cpConstants;

    private String colorprefix;

    public ActionMapping(RefboxClient refboxClient, RefboxConfig refboxConfig) {
        this.refboxClient = refboxClient;
    }

    public void updateCodedProblem(List<String> cpConstants) {
        this.cpConstants = cpConstants;
    }

    public SubProductionTask actionToTask (IntOp action) {
        String colorprefix = refboxClient.getTeamColor().map(x -> TeamColor.CYAN.equals(x) ? "C-": "M-").orElseThrow();

        String name = action.getName();
        SubProductionTask task;
        int orderIdIndex;
        int orderId;
        int robotIndex =  action.getInstantiations()[0];
        int robotId = Character.getNumericValue(cpConstants.get(robotIndex).charAt(1));

        Order order; 
        Base baseColor;
        
        int machineIdIndex;
        int machineId;
        Cap capColor;
        RingColor ringColor;

        switch (name) {

            // case "toyactiondeliveringfrombs":
            //     task = SubProductionTaskBuilder.newBuilder()
            //             .setName("toy")
            //             .setMachine(colorprefix + "BS")                 //ATAI: machine type can be BS, RS, CS or DS
            //             .setState(SubProductionTask.TaskState.TBD)
            //             .setType(SubProductionTask.TaskType.GET)        //ATAI: task type can be GET, DELIVER, BUFFER_CAP or MOVE (look in AspScheduler class for creation of move task. Right now they are automatically created.
            //             .setSide(MachineSide.Output)                    //ATAI: side can be INPUT, OUTPUT, SHELF or SLIDE
            //             .setOrderInfoId((long) orderId)                 //ATAI: this field is not required for "resource" tasks (like buffering the cap or retrieving a base to be delivered to the slide of a RS)
            //             .setRequiredColor(baseColor.toString())
            //             .setOptCode(null)
            //             .build();
            //     break;

            case "waitfordeliverywindow":
                orderIdIndex =  action.getInstantiations()[0];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("waitfordeliverywindow")
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DUMMY)
                        .setOrderInfoId((long) orderId)
                        .setOptCode(null)
                        .build();
                break;
            
            // Get a base from a BS for a product
            case "move_getbasefrombscriticaltask":
                orderIdIndex =  action.getInstantiations()[2];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                baseColor = order.getBase();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromBScriticalTask")
                        .setMachine(colorprefix + "BS")
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)
                        .setSide(MachineSide.Output)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(baseColor.toString())
                        .setOptCode(null)
                        .build();
                break;

            // Deliver a c0 product to a CS to mount the cap
            case "move_deliverproductc0tocscriticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                machineIdIndex = action.getValueOfParameter(2);
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                capColor = order.getCap();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC0ToCScriticalTask")
                        .setMachine(colorprefix + "CS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(capColor.toString())
                        .setOptCode("MountCap")
                        // .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .build();
                break;
                
            
            // Deliver a c1 product to a CS to mount the cap
            case "move_deliverproductc1tocscriticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                machineIdIndex = action.getValueOfParameter(2);
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                capColor = order.getCap();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC1ToCScriticalTask")
                        .setMachine(colorprefix + "CS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(capColor.toString())
                        // .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .setOptCode("MountCap")
                        .build();
                break;
                
            // Deliver a c2 product to a CS to mount the cap
            case "move_deliverproductc2tocscriticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                machineIdIndex = action.getValueOfParameter(2);
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                capColor = order.getCap();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC2ToCScriticalTask")
                        .setMachine(colorprefix + "CS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(capColor.toString())
                        // .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .setOptCode("MountCap")
                        .build();
                break;

            // Deliver a c3 product to a CS to mount the cap
            case "move_deliverproductc3tocscriticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                machineIdIndex = action.getValueOfParameter(2);
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                capColor = order.getCap();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductC3ToCScriticalTask")
                        .setMachine(colorprefix + "CS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(capColor.toString())
                        // .setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString())
                        .setOptCode("MountCap")
                        .build();
                break;
                
            // Buffer a cap at a CS
            case "move_getcapbasefromcsresourcetask":
                machineIdIndex = action.getInstantiations()[2];
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getcapbasefromcsresourcetask")
                        .setMachine(colorprefix + "CS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.BUFFER_CAP)
                        .setSide(MachineSide.Shelf)
                        .setIsDemandTask(true)
                        .setOptCode("RetrieveCap")
                        .build();
                break;
            
            // Get the product with the cap on from a CS
            case "move_getproductfromcscriticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                machineIdIndex = action.getValueOfParameter(2);
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getProductFromCScriticalTask")
                        .setMachine(colorprefix + "CS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)
                        .setSide(MachineSide.Output)
                        .setOrderInfoId((long) orderId)
                        .setOptCode(null)
                        .build();
                break;
                
            
            // Deliver a product to a RS to mount its 1st ring 
            case "move_deliverproducttors1criticaltask":
                machineIdIndex = action.getInstantiations()[1];
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                ringColor = order.getRing1().getColor();
                
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToRS1criticalTask")
                        .setMachine(colorprefix + "RS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(ringColor.toString())
                        .setOptCode(null)
                        .build();
                break;
            
            // Deliver a product to a RS to mount its 2nd ring 
            case "move_deliverproducttors2criticaltask":
                machineIdIndex = action.getInstantiations()[1];
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                ringColor = order.getRing2().getColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToRS2criticalTask")
                        .setMachine(colorprefix + "RS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(ringColor.toString())
                        .setOptCode(null)
                        .build();
                break;
            
            // Deliver a product to a RS to mount its 3rd ring 
            case "move_deliverproducttors3criticaltask":
                machineIdIndex = action.getInstantiations()[1];
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                ringColor = order.getRing3().getColor();
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToRS3criticalTask")
                        .setMachine(colorprefix + "RS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setRequiredColor(ringColor.toString())
                        .setOptCode(null)
                        .build();
                break;
                
            // Get the product with the ring mounted from a RS
            case "move_getproductfromrscriticaltask":
                orderIdIndex =  action.getInstantiations()[3];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                machineIdIndex = action.getInstantiations()[1];
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getproductfromrscriticaltask")
                        .setMachine(colorprefix + "RS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)
                        .setSide(MachineSide.Output)
                        .setOrderInfoId((long) orderId)
                        .setOptCode(null)
                        .build();
                break;
                
            // Get a base from the BS to get loaded in a RS
            case "move_getbasefrombsresourcetask":
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getBaseFromBSResourceTask")
                        .setMachine(colorprefix + "BS")
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)
                        .setSide(MachineSide.Output)
                        .setIsDemandTask(true)
                        .setOptCode(null)
                        .build();
                break;
                
            // Get a disposed base from a CS
            case "move_getbasefromcsresourcetask":
                machineIdIndex = action.getInstantiations()[2];
                machineId = Integer.parseInt(cpConstants.get(machineIdIndex).substring(2));
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("getbasefromcsresourcetask")
                        .setMachine(colorprefix + "CS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.GET)
                        .setSide(MachineSide.Output)
                        .setIsDemandTask(true)
                        .setOptCode(null)
                        .build();
                break;
            
            // Load a base in a RS slide
            case "move_deliverbasetorsresourcetask":
                machineIdIndex = action.getInstantiations()[1];
                machineId = Character.getNumericValue(cpConstants.get(machineIdIndex).charAt(2));
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverBaseToRSResourceTask")
                        .setMachine(colorprefix + "RS" + machineId)
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Slide)
                        .setIsDemandTask(true)
                        .setOptCode(null)
                        .build();
                break;
            
            // Deliver a product to the DS
            case "move_deliverproducttodscriticaltask":
                orderIdIndex =  action.getInstantiations()[2];
                orderId = Integer.parseInt(cpConstants.get(orderIdIndex).substring(1));
                order = refboxClient.getOrderById(orderId);
                task = SubProductionTaskBuilder.newBuilder()
                        .setName("deliverProductToDScriticalTask")
                        .setMachine(colorprefix + "DS")
                        .setState(SubProductionTask.TaskState.TBD)
                        .setType(SubProductionTask.TaskType.DELIVER)
                        .setSide(MachineSide.Input)
                        .setOrderInfoId((long) orderId)
                        .setOptCode(order.getDeliveryGate()+"")
                        .build();
                break;

            //ATAI parameters that have to be set for specific kind of tasks:
            //.setOrderInfoId((long) orderId) in all tasks in which you are manipulating a subpiece of the product (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide are excluded)
            //.setIsDemandTask(true) for all the tasks excluded by .setOrderInfoId (buffering cs, retrieving the base from the CS after the buffering, retrieving a base for a RS and delivering a base to the RS slide)
            //.setRequiredColor for retrieving a base, delivering partial product to RS to mount a ring, delivering partial product to CS to mount a cap
            //.setOptCode("RETRIEVE_CAP") for the buffer cap action
            //.setOptCode(MachineDescriptionProtos.CSOp.MOUNT_CAP.toString()) when delivering a partial product to a CS to mount a cap
            //.setOptCode(order.getDeliveryGate()+"") when delivering the final product to the DS (order is a ProductOrder object stored in the ProductOrderDAO)




            default:
                System.out.println("case not managed");
                task = null;
                break;
        }
        System.out.println("SETTING TASK ROBOT: r" + robotId);
        task.setRobotId(robotId);
        return task;
    }
}
