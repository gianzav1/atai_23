package com.grips.scheduler.asp;

import com.grips.config.ProductionTimesConfig;
import com.rcll.domain.*;
import com.grips.persistence.dao.*;
import com.grips.persistence.domain.Atom;
import com.grips.persistence.domain.MachineInfoRefBox;
import com.grips.persistence.domain.ProductOrder;
import com.rcll.planning.planparser.TemporalGraph;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.planning.encoding.Encoder;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.planner.Planner;
import com.rcll.planning.util.TempAtom;
import com.rcll.refbox.RefboxClient;
import fr.uga.pddl4j.parser.Domain;
import fr.uga.pddl4j.parser.Parser;
import fr.uga.pddl4j.parser.Problem;
import lombok.SneakyThrows;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.logging.log4j.core.config.OrderComparator;
import org.hibernate.mapping.Index;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@CommonsLog
public class PlanningThread {
    private final String domainS = "domain.pddl";
    private final String problemS = "problem";

    private final String plannerBinary = "../../tempo-sat-temporal-fast-downward/plan";
    // private final String plannerBinary = "../../optic-clp";
    private List<Order> goals;
    private TemporalGraph tgraph;

    private RcllCodedProblem cp;

    private final String threadNumber;
    private double makespawn;
    private final int remainingTime;
    private final boolean simplestPlan;

    private boolean noSolution;

    private Planner pl;

    private final KnowledgeBase kb;
    private final int numberRobots;

    private final MachineInfoRefBoxDao machineInfoRefBoxDao;

    private final ProductionTimesConfig productionTimes;
    private final TeamColor teamcolor;
    private final Map<String, Double> distanceMatrix;

    private final RefboxClient refboxClient;
    private double timeFactor = 0.25;   //approximated map between plan times and real times (not the simulation real time factor)

    public PlanningThread(String threadNumber, int remainingTime,
                          boolean simplestPlan, KnowledgeBase kb, int numberRobots,
                          ProductionTimesConfig productionTimesConfig, MachineInfoRefBoxDao machineInfoRefBoxDao,
                          TeamColor teamcolor, Map<String, Double> distanceMatrix, RefboxClient refboxClient) {
        this.productionTimes = productionTimesConfig;
        this.threadNumber = threadNumber;
        this.remainingTime = remainingTime;
        this.refboxClient = refboxClient;
        this.simplestPlan = simplestPlan;
        noSolution = false;
        this.kb = kb;
        this.numberRobots = numberRobots;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.teamcolor = teamcolor;
        this.distanceMatrix = distanceMatrix;
        createFolder();
    }


    public void plan() {
        deleteOldPlanFile();


        pl = null;

        log.info("Calling goal selector");
        goals = goalSelector();

        generateProblemFile(goals);
        log.info("Problem file generated");

        try {
            cp = callExtPlanner();
        } catch (Exception e) {
            if (e instanceof IOException) {
                log.error("Exception " + e.toString());
                makespawn = 100000;
                noSolution = true;
            }
        }

        collectResult();
        try {
            makespawn = tgraph.returnMakespawn();  //check makespan in case of plan not found
        } catch (NullPointerException e) {
            log.warn("Plan not found, setting makespan to 100000");
            makespawn = 100000;
            noSolution = true;
        }

    }

    @SneakyThrows
    public void createFolder() {
        ResourceHelper.copyResourcesFileTo("planning/rcll.pddl", "problem" + threadNumber + "/domain.pddl");
        new File("problem" + threadNumber + "/planning0").mkdirs();
    }

    public void collectResult() {
        String[] pathnames;

        int lastPlanningTime = findLastPlanningTimeFolder();
        File f = new File("problem" + threadNumber + "/planning" + lastPlanningTime);

        String lastPlan;
        int max_plan_index = 0; //plan not found
        pathnames = f.list();


        for (String pathname : pathnames) {
            //log.warn("pathname " + pathname);
            // Print the names of files and directories
            if (pathname.contains("sol")) {
                int plan_index = Integer.parseInt(pathname.substring(5));
                if (plan_index > max_plan_index)
                    max_plan_index = plan_index;
            }
        }

        if (max_plan_index > 0) {
            lastPlan = "/planning" + lastPlanningTime + "/sol" + threadNumber + "." + max_plan_index;
            reformatPlan(lastPlan, lastPlanningTime);
            readPlan(lastPlanningTime);


            Set<Double> keys = pl.getPlan().actions().keySet();
            Iterator<Double> itk = keys.iterator();
            while (itk.hasNext()) {
                Set<IntOp> ita = pl.getPlan().getActionSet(itk.next());
                Iterator<IntOp> acs = ita.iterator();
                while (acs.hasNext()) {
                    IntOp o = acs.next();
                    o.generateLists();
                }
            }
            log.warn("Generating temporal graph \n");
            //plan is represented as a temporal graph inside tg object
            TemporalGraph tg = new TemporalGraph(cp, pl.getPlan(), refboxClient);
            tg.createTemporalGraph();
            tg.normalizeActions();
            //tg.printOrderedActions();
            //tg.printGraph();
            log.warn("finish");
            this.tgraph = tg;
            deleteOldPlanFile();
        } else {
            log.warn("Plan not found in thread " + threadNumber);
        }

    }

    private int findLastPlanningTimeFolder() {
        String[] pathnames;

        File f = new File("problem" + threadNumber);
        pathnames = f.list();

        int lastPlanningTime = -1;

        for (String pathname : pathnames) {
            //log.warn("pathname " + pathname);
            // Print the names of files and directories
            if (pathname.contains("planning")) {
                int candidate = Integer.parseInt(pathname.substring(8));
                if (candidate > lastPlanningTime)
                    lastPlanningTime = candidate;
            }
        }
        return lastPlanningTime;
    }

    private void deleteOldPlanFile() {
        File f = new File("problem" + threadNumber);
        String[] pathnames = f.list();

        if (pathnames == null) {
            log.warn("Old folder not found, skipping deletion");
            return;
        }
        for (String pathname : pathnames) {
            //log.warn("pathname " + pathname);
            // Print the names of files and directories
            if (pathname.equals("output") || pathname.equals("plan") || pathname.equals("variables.groups") || pathname.equals("all.groups") || pathname.equals("output.sas")) {
                File myObj = new File("problem" + threadNumber + "/" + pathname);
                if (myObj.delete()) {
                    log.info("Deleted the file: " + myObj.getName());
                } else {
                    log.warn("Failed to delete the file.");
                }
            }
        }
    }

    private void readPlan(int lastPlanningTime) {
        File file = new File("problem" + threadNumber + "/planning" + lastPlanningTime + "/plan");
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            String s;

            List<IntOp> op = cp.getOperators();
            List<String> constants = cp.getConstants();

            while ((s = br.readLine()) != null) {
                String[] parsed = s.split(":");
                double time = Double.parseDouble(parsed[0]);
                int actionIndex = parsed[1].indexOf(")") + 1;
                String action = parsed[1].substring(1, actionIndex);
                double duration = Double.parseDouble(parsed[1].substring(actionIndex + 2, parsed[1].length() - 1));
                String actionName = action.split(" ")[0].substring(1);
                //adding grounded actions with time and duration
                if (!actionName.equals("waitfordeliverywindow")) {           //this is a toy action to encode temp constraint, not a real action
                    Iterator<IntOp> it = op.iterator();
                    IntOp grAction = null;
                    while (it.hasNext()) {
                        IntOp i = it.next();
                        if (i.getName().equals(actionName)) {
                            grAction = new IntOp(i);
                            String[] paramParsing = action.substring(0, action.length() - 1).split(" ");
                            String[] parameters = Arrays.copyOfRange(paramParsing, 1, paramParsing.length);
                            for (int j = 0; j < grAction.getInstantiations().length; j++) {
                                grAction.setValueOfParameter(j, constants.indexOf(parameters[j]));
                            }
                            grAction.setDuration(duration);
                            pl.addActionToPlan(time, grAction);
                        }
                    }
                }
            }
            fr.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void reformatPlan(String plan, int lastPlanningTime) {
        File file = new File("problem" + threadNumber + plan);
        TreeMap<Double, Set<String>> actionMap = new TreeMap<>();
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);  //creates a buffering character input stream
            String line;
            while ((line = br.readLine()) != null) {
                int separator = line.indexOf(":");
                addAction(actionMap, Double.parseDouble(line.substring(0, separator)), line.substring(separator + 2));
            }
            fr.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        File output = new File("problem" + threadNumber + "/planning" + lastPlanningTime + "/plan");
        try {
            FileWriter fr = new FileWriter(output, false);
            Set<Double> keys = actionMap.keySet();
            Iterator<Double> itk = keys.iterator();
            while (itk.hasNext()) {
                Double k = itk.next();
                Set<String> ita = actionMap.get(k);
                Iterator<String> acs = ita.iterator();
                while (acs.hasNext()) {
                    String buff = acs.next();
                    fr.write(k + ": " + buff + "\n");
                }
            }
            fr.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    public void addAction(TreeMap<Double, Set<String>> actionMap, Double key, String action) {
        Set<String> set = actionMap.get(key);
        if (set == null) {
            set = new HashSet<String>();
        }
        set.add(action);
        actionMap.put(key, set);
    }

    private RcllCodedProblem callExtPlanner() {

        //count the planning time

        //PARSING PHASE
        final StringBuilder strb = new StringBuilder();
        Parser parser = new Parser();
        try {
            String domainFile = "problem" + threadNumber + "/" + domainS;
            String problemFile = problemS + threadNumber + "/" + problemS + threadNumber + ".pddl";
            log.info("PARSING");
            parser.parse(domainFile, problemFile);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        log.warn("parsed\n");

        //ENCODING PHASE

        final Domain domain = parser.getDomain();
        final Problem problem = parser.getProblem();

        pl = new Planner(domainS, problemS + threadNumber + ".pddl", cp, plannerBinary);

        try {

            RcllCodedProblem cp = Encoder.encode(domain, problem);

            //PLANNING PHASE


            pl.plan(threadNumber);


            return cp;


        } catch (IllegalArgumentException ilException) {
            log.error("the problem to encode is not ADL, \":requirements\" not supported at this time\n");
            return null;
        }

    }

    public String getPlanString() {
        return pl.planToString(cp);
    }



    public ImmutablePair<List<Order>, ImmutablePair<TemporalGraph, RcllCodedProblem>> getResult() {
        return new ImmutablePair<>(goals, new ImmutablePair<>(tgraph, cp));
    }

    private void generateProblemFile(List<Order> goals) {
        try {
            File file = new File("problem" + threadNumber + "/" + problemS + threadNumber + ".pddl");
            FileWriter fr = new FileWriter(file, false);
            fr.write("(define (problem task)\n" +
                    "(:domain rcll)\n" +
                    "(:objects\n" +
                    "bs - base_station\n" +
                    "cs1_input cs2_input - cap_station_input\n" +
                    "cs1_output cs2_output - cap_station_output\n" +
                    "rs1_input rs2_input - ring_station_input\n" +
                    "rs1_output rs2_output - ring_station_output\n" +
                    "rs1 rs2 - ring_station\n" +
                    "cs1 cs2 - cap_station\n" +
                    "ds - delivery_station\n" +
                    "ring_green ring_yellow ring_blue ring_orange - rcolor\n" +
                    "cap_black cap_grey - ccolor\n" +
                    "zero one two three - numbers\n" +
                    "start - starting\n"
            );
            
            for (int i=1; i<=numberRobots; i++) { // TODO: adapt to number of robots
                fr.write("r" + i + " ");
            }
            
            fr.write("- robot\n");


            for (Order p : goals) {
                fr.write("p" + p.getId() + " ");
            }

            fr.write(" - product\n" +
                    ")\n" +
                    "(:init\n");


            kb.prettyPrintKBtoProblemFile(fr);

            generateGoalOnProblemFile(goals, fr);



            fr.write(")\n");
            fr.write("(:goal (and\n");
            for (Order p : goals) {
                fr.write("(delivered p" + p.getId() + ")\n");
            }
            fr.write(")\n" +
                    ")\n" +
                    // "(:metric minimize  (total-time) )\n" +
                    // "\n" +
                    ")");
            fr.close();

        } catch (IOException e) {
            log.error(e);
        }
    }

    //ATAI: given a set of goal chosen by the goalreasoner, model the related PDDL atoms here to be written in the problem file
    private void generateGoalOnProblemFile(List<Order> goals, FileWriter fr) throws IOException {
        for (Order p : goals) {

            long id = p.getId();
            ProductComplexity complexity = p.getComplexity();
            
            Atom stage = new Atom("stage_c0_0", "p" + id);
            if (this.kb.isNotInKB(stage.getName(), stage.getAttributes()) && 
                this.kb.isNotInKB("stage_c0_1", "p" + id) &&
                this.kb.isNotInKB("stage_c0_2", "p" + id) &&
                this.kb.isNotInKB("stage_c0_3", "p" + id)) {
                fr.write(stage.prettyPrinter());
            }

            Atom capColor = new Atom("capcolor", "p" + id + ";cap_" + p.getCap().name().toLowerCase());
            if (this.kb.isNotInKB(capColor.getName(), capColor.getAttributes())) {
                fr.write(capColor.prettyPrinter());
            }
            
            if (complexity == ProductComplexity.C1 || complexity == ProductComplexity.C2 || complexity == ProductComplexity.C3) {
                Atom ring1 = new Atom("ring1color", "p" + id + ";ring_" + p.getRing1().getColor().name().toLowerCase());
                if (this.kb.isNotInKB(ring1.getName(), ring1.getAttributes())) {
                    fr.write(ring1.prettyPrinter());
                }
            }

            if (complexity == ProductComplexity.C2 || complexity == ProductComplexity.C3) {
                Atom ring2 = new Atom("ring2color", "p" + id + ";ring_" + p.getRing2().getColor().name().toLowerCase());
                if (this.kb.isNotInKB(ring2.getName(), ring2.getAttributes())) {
                    fr.write(ring2.prettyPrinter());
                }
            }

            if (complexity == ProductComplexity.C3) {
                Atom ring3 = new Atom("ring3color", "p" + id + ";ring_" + p.getRing3().getColor().name().toLowerCase());
                if (this.kb.isNotInKB(ring3.getName(), ring3.getAttributes())) {
                    fr.write(new Atom("ring3color", "p" + id + ";ring_" + p.getRing3().getColor().name().toLowerCase()).prettyPrinter());
                }
            }
            
            Atom tocheck = new Atom("tocheck", "p" + id);
            if (this.kb.isNotInKB(tocheck.getName(), tocheck.getAttributes())) {
                fr.write(tocheck.prettyPrinter());
            }

            switch (complexity) {
                case C0:
                    Atom complexity0 = new Atom("complexity0", "p" + id);
                    if (this.kb.isNotInKB(complexity0.getName(), complexity0.getAttributes())) {
                        fr.write(complexity0.prettyPrinter());
                    }
                    break;
                case C1:
                    Atom complexity1 = new Atom("complexity1", "p" + id);
                    if (this.kb.isNotInKB(complexity1.getName(), complexity1.getAttributes())) {
                        fr.write(complexity1.prettyPrinter());
                    }
                    break;
                case C2:
                    Atom complexity2 = new Atom("complexity2", "p" + id);
                    if (this.kb.isNotInKB(complexity2.getName(), complexity2.getAttributes())) {
                        fr.write(complexity2.prettyPrinter());
                    }
                    break;
                case C3:
                    Atom complexity3 = new Atom("complexity3", "p" + id);
                    if (this.kb.isNotInKB(complexity3.getName(), complexity3.getAttributes())) {
                        fr.write(complexity3.prettyPrinter());
                    }
                    break;
            }
            
            if (p.getDeliveryPeriodBegin() == 0) {
                Atom deliveryWindowStart = new Atom("deliveryWindowStart", "p" + id, 0.1);
                if (this.kb.isNotInKB(deliveryWindowStart.getName(), deliveryWindowStart.getAttributes())) {
                    fr.write(deliveryWindowStart.prettyPrinter());
                }
            } else {
                Atom deliveryWindowStart = new Atom("deliveryWindowStart", "p" + id, p.getDeliveryPeriodBegin());
                if (this.kb.isNotInKB(deliveryWindowStart.getName(), deliveryWindowStart.getAttributes())) {
                    fr.write(deliveryWindowStart.prettyPrinter());
                }
            }
        }
    }

    //ATAI: implement the goalSelector here
    private List<Order> goalSelector() { //stupid goal select: return the first order
        log.info("STARTED GOAL REASONER");
        List<Order> toPlan = new ArrayList<>();
        List <Order> orders = refboxClient.getAllOrders();

        /* Check first if there are already products held by robots or loaded into machines,
         * if it's the case, plan for their delivery
         */
        List <Order> productsInProduction = this.kb.getProductsInProduction();

        int ordersToProduce = 0;
        int maxOrdersToProduce = 2;
        
        if (!productsInProduction.isEmpty()) {
            productsInProduction.forEach(o -> toPlan.add(o));
            ordersToProduce += productsInProduction.size();
        }
        
        if (ordersToProduce >= maxOrdersToProduce) {
            return toPlan;
        }

        // Keep only the orders that can actually be produced before the time runs out
        
        List <Order> doableOrders = orders.stream().filter(o -> this.enoughTimeToComplete(o, numberRobots)).collect(Collectors.toList());
        
        if (doableOrders.isEmpty()) {
            log.error("No product can be delivered in time! Empty goal!");
            return doableOrders;
        }

        Iterator <Order> c0orders = doableOrders.stream().filter(o -> o.getComplexity().equals(ProductComplexity.C0)).collect(Collectors.toList()).iterator();
        Iterator <Order> c1orders = doableOrders.stream().filter(o -> o.getComplexity().equals(ProductComplexity.C1)).collect(Collectors.toList()).iterator();
        Iterator <Order> c2orders = doableOrders.stream().filter(o -> o.getComplexity().equals(ProductComplexity.C2)).collect(Collectors.toList()).iterator();
        Iterator <Order> c3orders = doableOrders.stream().filter(o -> o.getComplexity().equals(ProductComplexity.C3)).collect(Collectors.toList()).iterator();
        
        
        
        // plan for a maximum of 2 orders at a time
        while (ordersToProduce < maxOrdersToProduce && c3orders.hasNext()) {
            toPlan.add(c3orders.next());
            ordersToProduce++;
        }

        while (ordersToProduce < maxOrdersToProduce && c2orders.hasNext()) {
            toPlan.add(c2orders.next());
            ordersToProduce++;
        }

        while (ordersToProduce < maxOrdersToProduce && c1orders.hasNext()) {
            toPlan.add(c1orders.next());
            ordersToProduce++;
        }

        while (ordersToProduce < maxOrdersToProduce && c0orders.hasNext()) {
            toPlan.add(c0orders.next());
            ordersToProduce++;
        }

        
        return toPlan;
    }


    /*
     * Compute if a given product can be delivered before its delivery window ends
     * @param o the order
     * @param numberRobots the number of robots used in the game
     * @return true if the product can be delivered in time
     */
    private boolean enoughTimeToComplete(Order o, int numberRobots) {
        ProductComplexity complexity = o.getComplexity();
        /*
         * 0 = no base
         * 1 = got base
         * 2 = first ring mounted or got cap for c0
         * 3 = second ring mounted or got cap for c1
         * 4 = third ring mounted or got cap for c2
         * 5 = cap mounted for c3
         */
        int orderState = this.kb.getOrderState(o);

        int totalBasesToPay;
        // move = 15, normal = 20, buffer = 25
        int timeToComplete = productionTimes.getMoveTask()  // move to cap station
                + productionTimes.getBufferTask()           // buffer cap
                + productionTimes.getMoveTask()             // move to grab disposed base
                + productionTimes.getNormalTask()           // grab disposed base
                + productionTimes.getMoveTask()             // move to ring station
                + productionTimes.getNormalTask()           // pay base
                + productionTimes.getMoveTask()             // move to base station
                + productionTimes.getNormalTask()           // grab base
                + productionTimes.getMoveTask()             // move to cap station
                + productionTimes.getNormalTask()           // put base into cap station
                + productionTimes.getMoveTask()             // move to cap station output
                + productionTimes.getNormalTask()           // grab completed product
                + productionTimes.getMoveTask()             // move to delivery station
                + productionTimes.getNormalTask();          // deliver product

        int payBaseIncrement = productionTimes.getMoveTask()        // get to a place to take a base
                             + productionTimes.getNormalTask()      // get the base
                             + productionTimes.getMoveTask()        // move to a ring station
                             + productionTimes.getNormalTask();     // pay the base
        int mountRingIncrement = productionTimes.getMoveTask()      // go to take the product
                               + productionTimes.getNormalTask()    // grab the product
                               + productionTimes.getMoveTask()      // go to a ring station to mount a ring
                               + productionTimes.getNormalTask();   // mount the ring

        switch (complexity) {
            case C1:
                totalBasesToPay = o.getRing1().getRawMaterial();
                break;
            case C2:
                totalBasesToPay = o.getRing1().getRawMaterial() + o.getRing2().getRawMaterial();
                break;
            case C3:
                totalBasesToPay = o.getRing1().getRawMaterial() + o.getRing2().getRawMaterial()
                        + o.getRing3().getRawMaterial();
                break;
            default:
                totalBasesToPay = 0;
                break;
        }

        timeToComplete += payBaseIncrement * totalBasesToPay + complexity.ordinal() * mountRingIncrement;
        timeToComplete -= orderState * (productionTimes.getMoveTask() + productionTimes.getNormalTask()); 
        timeToComplete = timeToComplete / numberRobots;

        if (refboxClient.getLatestGameTimeInSeconds() + timeToComplete <= o.getDeliveryPeriodEnd()) {
            return true;
        } else {
            return false;
        }
    }


    public void writeDistance(FileWriter fr, String from, String fromCompleteName, MachineName to) throws IOException {
        String nameTo = to.getRawMachineName();
        if (to.isCapStation()) {
            if (!fromCompleteName.equals(nameTo + "_input"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_input", distanceMatrix.get(fromCompleteName + nameTo + "_input"), false);
            if (!fromCompleteName.equals(nameTo + "_output"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_output", distanceMatrix.get(fromCompleteName + nameTo + "_output"), false);
            if (!fromCompleteName.equals(nameTo + "_shelf"))
                writeDistanceCheck(fr, from, "cs" + nameTo.charAt(4) + "_shelf", distanceMatrix.get(fromCompleteName + nameTo + "_shelf"), true);
        } else if (to.isRingStation()) {
            if (!fromCompleteName.equals(nameTo + "_input"))
                writeDistanceCheck(fr, from, "rs" + nameTo.charAt(4) + "_input", distanceMatrix.get(fromCompleteName + nameTo + "_input"), false);
            if (!fromCompleteName.equals(nameTo + "_output"))
                writeDistanceCheck(fr, from, "rs" + nameTo.charAt(4) + "_output", distanceMatrix.get(fromCompleteName + nameTo + "_output"), false);
        } else if (to.isBaseStation() && !fromCompleteName.equals(nameTo + "_output")) {
            writeDistanceCheck(fr, from, nameTo.substring(2).toLowerCase(Locale.ROOT), distanceMatrix.get(fromCompleteName + nameTo + "_output"), false);
        } else if (to.isDeliveryStation() && !fromCompleteName.equals(nameTo + "_input")) {
            writeDistanceCheck(fr, from, nameTo.substring(2).toLowerCase(Locale.ROOT), distanceMatrix.get(fromCompleteName + nameTo + "_input"), false);
        }
    }

    public void writeDistanceCheck(FileWriter fr, String from, String to, double cost, boolean isBufferTask) throws IOException {
        int seconds = (int) (cost * timeFactor);
        if (!from.equals(to)) {
            if (isBufferTask)
                fr.write("( = (distanceBuffer " + from + " " + to + ") " + (seconds + productionTimes.getBufferTask()) + ")\n ");
            else
                fr.write("( = (distance " + from + " " + to + ") " + (seconds + productionTimes.getNormalTask()) + ")\n ");
            //System.out.print(" = (distance " + from + " " + to + ") " + cost + ")\n ");
        }
    }
}