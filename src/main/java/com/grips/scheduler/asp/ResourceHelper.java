package com.grips.scheduler.asp;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ResourceHelper {
    public static void copyResourcesFileTo(String resourceName, String runtimeName) throws IOException {
        URL inputUrl = ResourceHelper.class.getClassLoader().getResource(resourceName);
        File dest = new File(runtimeName);
        FileUtils.copyURLToFile(inputUrl, dest);
    }
}
