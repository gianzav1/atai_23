package com.grips.scheduler.asp;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.grips.config.ProductionConfig;
import com.grips.persistence.dao.AtomDao;
import com.grips.persistence.dao.MachineInfoRefBoxDao;
import com.grips.persistence.domain.Atom;
import com.rcll.domain.Order;
import com.rcll.domain.ProductComplexity;
import com.rcll.domain.RingColor;
import com.rcll.planning.encoding.IntOp;
import com.rcll.planning.encoding.RcllCodedProblem;
import com.rcll.refbox.RefboxClient;

import fr.uga.pddl4j.util.IntExp;
import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
@Service
public class KnowledgeBase {

    private final AtomDao atomDao;
    @Value("${gameconfig.planningparameters.numberofrobots}")
    private int numberRobots;
    private final MachineInfoRefBoxDao machineInfoRefBoxDao;
    private RcllCodedProblem cp;


    @Autowired
    private ProductionConfig productionConfig;

    @Value("${gameconfig.productiontimes.normalTask}")
    private int normalTaskDuration;
    @Value("${gameconfig.productiontimes.bufferTask}")
    private int bufferTaskDuration;
    @Value("${gameconfig.productiontimes.moveTask}")
    private int moveTaskDuration;

    private final RefboxClient refboxClient;
    public  KnowledgeBase(AtomDao atomDao, MachineInfoRefBoxDao machineInfoRefBoxDao, ProductionConfig productionConfig,
                          RefboxClient refboxClient) {
        this.atomDao = atomDao;
        this.machineInfoRefBoxDao = machineInfoRefBoxDao;
        this.productionConfig = productionConfig;
        this.refboxClient = refboxClient;
    }

    //ATAI: define the initial atoms of the KB here
    public void generateInitialKB() {
        for (int i=1; i<=numberRobots; i++) { // TODO: adapt to number of robots
            this.add(new Atom("at", "r" + i + ";start"));
            this.add(new Atom("free", "r" + i));
            this.add(new Atom("n_holding", "r" + i));
        }
        

        this.add(new Atom("empty", "cs1_input"));
        this.add(new Atom("empty", "cs1_output"));
        this.add(new Atom("n_station_holding", "cs1"));
        this.add(new Atom("n_cs_station_has_capbase", "cs1"));
        this.add(new Atom("parent_cs", "cs1_input;cs1"));
        this.add(new Atom("parent_cs", "cs1_output;cs1"));
        this.add(new Atom("n_basetodispose", "cs1"));

        this.add(new Atom("empty", "cs2_input"));
        this.add(new Atom("empty", "cs2_output"));
        this.add(new Atom("n_station_holding", "cs2"));
        this.add(new Atom("n_cs_station_has_capbase", "cs2"));
        this.add(new Atom("parent_cs", "cs2_input;cs2"));
        this.add(new Atom("parent_cs", "cs2_output;cs2"));
        this.add(new Atom("n_basetodispose", "cs2"));

        this.add(new Atom("empty", "rs1_input"));
        this.add(new Atom("empty", "rs1_output"));
        this.add(new Atom("n_station_holding", "rs1"));
        this.add(new Atom("parent_rs", "rs1_input;rs1"));
        this.add(new Atom("parent_rs", "rs1_output;rs1"));

        this.add(new Atom("empty", "rs2_input"));
        this.add(new Atom("empty", "rs2_output"));
        this.add(new Atom("n_station_holding", "rs2"));
        this.add(new Atom("parent_rs", "rs2_input;rs2"));
        this.add(new Atom("parent_rs", "rs2_output;rs2"));

        this.add(new Atom("empty", "bs"));
        this.add(new Atom("empty", "ds"));
        



        this.add(new Atom("stationcapcolor", productionConfig.getBlack_cap_machine().toLowerCase(Locale.ROOT) + ";cap_black"));
        this.add(new Atom("stationcapcolor", productionConfig.getGrey_cap_machine().toLowerCase(Locale.ROOT) + ";cap_grey"));

        this.add(new Atom("stationringcolor", "rs1;" + ringToColorInPddl(machineInfoRefBoxDao.findByName("C-RS1").getRing1())));
        this.add(new Atom("stationringcolor", "rs1;" + ringToColorInPddl(machineInfoRefBoxDao.findByName("C-RS1").getRing2())));
        this.add(new Atom("stationringcolor", "rs2;" + ringToColorInPddl(machineInfoRefBoxDao.findByName("C-RS2").getRing1())));
        this.add(new Atom("stationringcolor", "rs2;" + ringToColorInPddl(machineInfoRefBoxDao.findByName("C-RS2").getRing2())));

        this.add(new Atom("loadedbases", "rs1;zero"));
        this.add(new Atom("loadedbases", "rs2;zero"));

        this.add(new Atom("requiredbases", "ring_blue;" + intToString(refboxClient.getRingByColor(RingColor.Blue).getRawMaterial())));
        this.add(new Atom("requiredbases", "ring_green;" + intToString(refboxClient.getRingByColor(RingColor.Green).getRawMaterial())));
        this.add(new Atom("requiredbases", "ring_orange;" + intToString(refboxClient.getRingByColor(RingColor.Orange).getRawMaterial())));
        this.add(new Atom("requiredbases", "ring_yellow;" + intToString(refboxClient.getRingByColor(RingColor.Yellow).getRawMaterial())));

        this.add(new Atom("increasebyone", "zero;one"));
        this.add(new Atom("increasebyone", "one;two"));
        this.add(new Atom("increasebyone", "two;three"));

        this.add(new Atom("subtract", "zero;zero;zero"));
        // this.add(new Atom("subtract", "zero;one;zero"));
        // this.add(new Atom("subtract", "zero;two;zero"));
        // this.add(new Atom("subtract", "zero;three;zero"));

        this.add(new Atom("subtract", "one;zero;one"));
        this.add(new Atom("subtract", "one;one;zero"));
        // this.add(new Atom("subtract", "one;two;zero"));
        // this.add(new Atom("subtract", "one;three;zero"));

        this.add(new Atom("subtract", "two;zero;two"));
        this.add(new Atom("subtract", "two;one;one"));
        this.add(new Atom("subtract", "two;two;zero"));
        // this.add(new Atom("subtract", "two;three;zero"));

        this.add(new Atom("subtract", "three;zero;three"));
        this.add(new Atom("subtract", "three;one;two"));
        this.add(new Atom("subtract", "three;two;one"));
        this.add(new Atom("subtract", "three;three;zero"));



    }

    private String ringToColorInPddl(RingColor ringColor) {
        switch (ringColor) {
            case Blue:
                return "ring_blue";
            case Green:
                return "ring_green";
            case Orange:
                return "ring_orange";
            case Yellow:
                return "ring_yellow";
        }
        throw new IllegalArgumentException("Unkown ring color: " + ringColor);
    }

    //ATAI: given a set of goal chosen by the goalreasoner, model the related KB atoms here
    public void generateOrderKb (List<Order> goals) {
        for (Order p : goals) {

            long id = p.getId();
            ProductComplexity complexity = p.getComplexity();
            
            Atom stage = new Atom("stage_c0_0", "p" + id);
            if (this.isNotInKB(stage.getName(), stage.getAttributes()) && 
                this.isNotInKB("stage_c0_1", "p" + id) &&
                this.isNotInKB("stage_c0_2", "p" + id) &&
                this.isNotInKB("stage_c0_3", "p" + id)) {
                this.add(stage);
            }

            Atom capColor = new Atom("capcolor", "p" + id + ";cap_" + p.getCap().name().toLowerCase());
            if (this.isNotInKB(capColor.getName(), capColor.getAttributes())) {
                this.add(capColor);
            }
            
            if (complexity == ProductComplexity.C1 || complexity == ProductComplexity.C2 || complexity == ProductComplexity.C3) {
                Atom ring1 = new Atom("ring1color", "p" + id + ";ring_" + p.getRing1().getColor().name().toLowerCase());
                if (this.isNotInKB(ring1.getName(), ring1.getAttributes())) {
                    this.add(ring1);
                }
            }

            if (complexity == ProductComplexity.C2 || complexity == ProductComplexity.C3) {
                Atom ring2 = new Atom("ring2color", "p" + id + ";ring_" + p.getRing2().getColor().name().toLowerCase());
                if (this.isNotInKB(ring2.getName(), ring2.getAttributes())) {
                    this.add(ring2);
                }
            }

            if (complexity == ProductComplexity.C3) {
                Atom ring3 = new Atom("ring3color", "p" + id + ";ring_" + p.getRing3().getColor().name().toLowerCase());
                if (this.isNotInKB(ring3.getName(), ring3.getAttributes())) {
                    this.add(new Atom("ring3color", "p" + id + ";ring_" + p.getRing3().getColor().name().toLowerCase()));
                }
            }
            
            Atom tocheck = new Atom("tocheck", "p" + id);
            if (this.isNotInKB(tocheck.getName(), tocheck.getAttributes())) {
                this.add(tocheck);
            }

            switch (complexity) {
                case C0:
                    Atom complexity0 = new Atom("complexity0", "p" + id);
                    if (this.isNotInKB(complexity0.getName(), complexity0.getAttributes())) {
                        this.add(complexity0);
                    }
                    break;
                case C1:
                    Atom complexity1 = new Atom("complexity1", "p" + id);
                    if (this.isNotInKB(complexity1.getName(), complexity1.getAttributes())) {
                        this.add(complexity1);
                    }
                    break;
                case C2:
                    Atom complexity2 = new Atom("complexity2", "p" + id);
                    if (this.isNotInKB(complexity2.getName(), complexity2.getAttributes())) {
                        this.add(complexity2);
                    }
                    break;
                case C3:
                    Atom complexity3 = new Atom("complexity3", "p" + id);
                    if (this.isNotInKB(complexity3.getName(), complexity3.getAttributes())) {
                        this.add(complexity3);
                    }
                    break;
            }
            
            if (p.getDeliveryPeriodBegin() == 0) {
                Atom deliveryWindowStart = new Atom("deliveryWindowStart", "p" + id, 0.1);
                if (this.isNotInKB(deliveryWindowStart.getName(), deliveryWindowStart.getAttributes())) {
                    this.add(deliveryWindowStart);
                }
            } else {
                Atom deliveryWindowStart = new Atom("deliveryWindowStart", "p" + id, p.getDeliveryPeriodBegin());
                if (this.isNotInKB(deliveryWindowStart.getName(), deliveryWindowStart.getAttributes())) {
                    this.add(deliveryWindowStart);
                }
            }
        }

    }


    public void printKB () {
        for( Atom at : atomDao.findAll())
            System.out.println(at.toString());
    }


    public void prettyPrintKBtoProblemFile (FileWriter fr) throws IOException {
        for (Atom at : atomDao.findAll())
            if(!at.getName().equals("holdafter") )
                fr.write(at.prettyPrinter());
    }


    public void add (Atom at) {
        if (atomDao.findByNameAndAttributes(at.getName(), at.getAttributes()).size() == 0 )
            atomDao.save(at);
        else
            log.warn("Trying to add to KB atom " + at.toString() + " already present");

    }



    public boolean isNotInKB (String pd, String attributes) {
        return atomDao.findByNameAndAttributes(pd, attributes).isEmpty();
    }

    public void setCp (RcllCodedProblem cp) {
        this.cp = cp;
    }


    public String intToString (int number) {
        switch (number) {

            case 0:
                return "zero";

            case 1:
                return "one";

            case 2:
                return "two";

            case 3:
                return "three";

            default:
                return null;

        }
    }


    public void purgeKB () {
        atomDao.deleteAll();
    }


    /**
     * check if start precondition of action are satisfied
     * @param action
     * @return true if all the preconditions are present in the KB
     */
    public Boolean checkStartPreconditions(IntOp action) {
        log.info("Checking starting preconditions for action " + action.getName());
        List<IntExp> preconditions = action.getPos_pred_atstart(); 
        

        for (IntExp condition : preconditions) {
            String name = cp.getPredicates().get(condition.getPredicate());
            String args = "";

            for (int arg : condition.getArguments()) {
                args += cp.getConstants().get(arg) + ";";
            }
            args = args.substring(0, args.length()-1);

            if (atomDao.findByNameAndAttributes(name, args).isEmpty()) {
                log.warn("Atom (" + name + " " + args + ")" + "not present in KB");
                
                for (Atom current : atomDao.findAll()) {
                    System.out.println(current.getId() + ": (" + current.getName() + " " + current.getAttributes()+ ")"); 
                }
                return false;
            }
        }

        return true;
    }





    /**
     * check if end precondition of action are satisfied
     * @param action
     * @return true if all the preconditions are present in the KB
     */
    public Boolean checkEndPreconditions(IntOp action) {
        log.info("Checking ending preconditions for action " + action.getName());
        List<IntExp> preconditions = action.getPos_pred_atend(); 
        
        for (IntExp condition : preconditions) {
            String name = cp.getPredicates().get(condition.getPredicate());
            String args = "";

            for (int arg : condition.getArguments()) {
                args += cp.getConstants().get(arg) + ";";
            }
            args = args.substring(0, args.length()-1);

            if (atomDao.findByNameAndAttributes(name, args).isEmpty()) {
                log.warn("Atom (" + name + " " + args + ")" + "not present in KB");
                
                for (Atom current : atomDao.findAll()) {
                    System.out.println(current.getId() + ": (" + current.getName() + " " + current.getAttributes()+ ")"); 
                }
                return false;
            }
        }

        return true;
    }

    /*
     * Apply the starting effects of an action
     * @param action
     */
    public void applyStartEffects(IntOp action) {
        List <IntExp> posEffects = action.getPos_eff_atstart();
        List <IntExp> negEffects = action.getNeg_eff_atstart();
        

        for (IntExp eff : negEffects) {
            String name = cp.getPredicates().get(eff.getPredicate());
            String args = "";
            

            for (int arg : eff.getArguments()) {
                args += cp.getConstants().get(arg) + ";";
            }
            args = args.substring(0, args.length()-1);
            
            if (name == "distance" || name == "distanceBuffer" || name == "deliveryWindowStart") {
                double value = eff.getValue();
                System.out.println("START NEGATIVE EFFECT: (= (" + name + " " + args + ")" + value + ")");
                // atomDao.delete(new Atom(name, args, value));
                if (!atomDao.findByNameAndAttributes(name, args).isEmpty()) {
                    atomDao.delete(atomDao.findByNameAndAttributes(name, args).get(0));
                }
            } else {
                System.out.println("START NEGATIVE EFFECT: (" + name + " " + args + ")");
                // atomDao.delete(new Atom(name, args));
                if (!atomDao.findByNameAndAttributes(name, args).isEmpty()) {
                    atomDao.delete(atomDao.findByNameAndAttributes(name, args).get(0));
                }
            }
        }

        for (IntExp eff : posEffects) {
            String name = cp.getPredicates().get(eff.getPredicate());
            String args = "";

            for (int arg : eff.getArguments()) {
                args += cp.getConstants().get(arg) + ";";
            }
            args = args.substring(0, args.length()-1);
            

            if (name == "distance" || name == "distanceBuffer" || name == "deliveryWindowStart") {
                double value = eff.getValue();
                System.out.println("START POSITIVE EFFECT: (= (" + name + " " + args + ")" + value + ")");
                Atom toAdd = new Atom(name, args, value);
                if (this.isNotInKB(toAdd.getName(), toAdd.getAttributes())) {
                    atomDao.save(toAdd);
                }
            } else {
                System.out.println("START POSITIVE EFFECT: (" + name + " " + args + ")");
                Atom toAdd = new Atom(name, args);
                if (this.isNotInKB(toAdd.getName(), toAdd.getAttributes())) {
                    atomDao.save(toAdd);
                }
            }
        }
        
    }

    /*
     * Apply the ending effects of an action
     * @param action
     */
    public void applyEndEffects(IntOp action) {
        List <IntExp> posEffects = action.getPos_eff_atend();
        List <IntExp> negEffects = action.getNeg_eff_atend();
        

        for (IntExp eff : negEffects) {
            String name = cp.getPredicates().get(eff.getPredicate());
            String args = "";

            for (int arg : eff.getArguments()) {
                args += cp.getConstants().get(arg) + ";";
            }
            args = args.substring(0, args.length()-1);
            

            if (name == "distance" || name == "distanceBuffer" || name == "deliveryWindowStart") {
                double value = eff.getValue();
                System.out.println("END NEGATIVE EFFECT: (= (" + name + " " + args + ")" + value + ")");
                // atomDao.delete(new Atom(name, args, value));
                if (!atomDao.findByNameAndAttributes(name, args).isEmpty()) {
                    atomDao.delete(atomDao.findByNameAndAttributes(name, args).get(0));
                }
            } else {
                System.out.println("END NEGATIVE EFFECT: (" + name + " " + args + ")");
                // atomDao.delete(new Atom(name, args));
                if (!atomDao.findByNameAndAttributes(name, args).isEmpty()) {
                    atomDao.delete(atomDao.findByNameAndAttributes(name, args).get(0));
                }
            }
        }

        for (IntExp eff : posEffects) {
            String name = cp.getPredicates().get(eff.getPredicate());
            String args = "";

            for (int arg : eff.getArguments()) {
                args += cp.getConstants().get(arg) + ";";
            }
            args = args.substring(0, args.length()-1);
            

            if (name == "distance" || name == "distanceBuffer" || name == "deliveryWindowStart") {
                double value = eff.getValue();
                System.out.println("END POSITIVE EFFECT: (= (" + name + " " + args + ")" + value + ")");
                Atom toAdd = new Atom(name, args, value);
                if (this.isNotInKB(toAdd.getName(), toAdd.getAttributes())) {
                    atomDao.save(toAdd);
                }
            } else {
                System.out.println("END POSITIVE EFFECT: (" + name + " " + args + ")");
                Atom toAdd = new Atom(name, args);
                if (this.isNotInKB(toAdd.getName(), toAdd.getAttributes())) {
                    atomDao.save(toAdd);
                }
            }
        }
        
    }

    /*
     * Add the distances between game zones to the Knowledge Base
     * @param distances is a map from zone*zone -> distance
     */
    public void generateDistancesKB(Map<String, Double> distances) {

        this.add(new Atom("distance", "start;start", 0));
        this.add(new Atom("distance", "start;bs", distances.get("startC-BS_output")));
        this.add(new Atom("distance", "start;ds", distances.get("startC-DS_input")));
        this.add(new Atom("distance", "start;cs1_input", distances.get("startC-CS1_input")));
        this.add(new Atom("distance", "start;cs1_output", distances.get("startC-CS1_output")));
        this.add(new Atom("distance", "start;cs2_input", distances.get("startC-CS2_input")));
        this.add(new Atom("distance", "start;cs2_output", distances.get("startC-CS2_output")));
        this.add(new Atom("distance", "start;rs1_input", distances.get("startC-RS1_input")));
        this.add(new Atom("distance", "start;rs1_output", distances.get("startC-RS1_output")));
        this.add(new Atom("distance", "start;rs2_input", distances.get("startC-RS2_input")));
        this.add(new Atom("distance", "start;rs2_output", distances.get("startC-RS2_output")));
        this.add(new Atom("distanceBuffer", "start;cs1_input", distances.get("startC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "start;cs2_input", distances.get("startC-CS2_shelf")));
        


        this.add(new Atom("distance", "bs;start", distances.get("startC-BS_output")));
        this.add(new Atom("distance", "cs1_input;start", distances.get("startC-CS1_input")));
        this.add(new Atom("distance", "cs1_output;start", distances.get("startC-CS1_output")));
        this.add(new Atom("distance", "cs2_input;start", distances.get("startC-CS2_input")));
        this.add(new Atom("distance", "cs2_output;start", distances.get("startC-CS2_output")));
        this.add(new Atom("distance", "rs1_input;start", distances.get("startC-RS1_input")));
        this.add(new Atom("distance", "rs1_output;start", distances.get("startC-RS1_output")));
        this.add(new Atom("distance", "rs2_input;start", distances.get("startC-RS2_input")));
        this.add(new Atom("distance", "rs2_output;start", distances.get("startC-RS2_output")));
        this.add(new Atom("distance", "ds;start", distances.get("startC-DS_input")));


        this.add(new Atom("distance", "bs;bs", 0));
        this.add(new Atom("distance", "bs;ds", distances.get("C-BS_outputC-DS_input")));
        this.add(new Atom("distance", "bs;cs1_input", distances.get("C-BS_outputC-CS1_input")));
        this.add(new Atom("distance", "bs;cs1_output", distances.get("C-BS_outputC-CS1_output")));
        this.add(new Atom("distance", "bs;cs2_input", distances.get("C-BS_outputC-CS2_input")));
        this.add(new Atom("distance", "bs;cs2_output", distances.get("C-BS_outputC-CS2_output")));
        this.add(new Atom("distance", "bs;rs1_input", distances.get("C-BS_outputC-RS1_input")));
        this.add(new Atom("distance", "bs;rs1_output", distances.get("C-BS_outputC-RS1_output")));
        this.add(new Atom("distance", "bs;rs2_input", distances.get("C-BS_outputC-RS2_input")));
        this.add(new Atom("distance", "bs;rs2_output", distances.get("C-BS_outputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "bs;cs1_input", distances.get("C-BS_outputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "bs;cs2_input", distances.get("C-BS_outputC-CS2_shelf")));

        this.add(new Atom("distance", "cs1_input;bs", distances.get("C-CS1_inputC-BS_output")));
        this.add(new Atom("distance", "cs1_input;ds", distances.get("C-CS1_inputC-DS_input")));
        this.add(new Atom("distance", "cs1_input;cs1_input", 0));
        this.add(new Atom("distance", "cs1_input;cs1_output", distances.get("C-CS1_inputC-CS1_output")));
        this.add(new Atom("distance", "cs1_input;cs2_input", distances.get("C-CS1_inputC-CS2_input")));
        this.add(new Atom("distance", "cs1_input;cs2_output", distances.get("C-CS1_inputC-CS2_output")));
        this.add(new Atom("distance", "cs1_input;rs1_input", distances.get("C-CS1_inputC-RS1_input")));
        this.add(new Atom("distance", "cs1_input;rs1_output", distances.get("C-CS1_inputC-RS1_output")));
        this.add(new Atom("distance", "cs1_input;rs2_input", distances.get("C-CS1_inputC-RS2_input")));
        this.add(new Atom("distance", "cs1_input;rs2_output", distances.get("C-CS1_inputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "cs1_input;cs1_input", distances.get("C-CS1_inputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "cs1_input;cs2_input", distances.get("C-CS1_inputC-CS2_shelf")));

        this.add(new Atom("distance", "cs1_output;bs", distances.get("C-CS1_outputC-BS_output")));
        this.add(new Atom("distance", "cs1_output;ds", distances.get("C-CS1_outputC-DS_input")));
        this.add(new Atom("distance", "cs1_output;cs1_input", distances.get("C-CS1_outputC-CS1_input")));
        this.add(new Atom("distance", "cs1_output;cs1_output", 0));
        this.add(new Atom("distance", "cs1_output;cs2_input", distances.get("C-CS1_outputC-CS2_input")));
        this.add(new Atom("distance", "cs1_output;cs2_output", distances.get("C-CS1_outputC-CS2_output")));
        this.add(new Atom("distance", "cs1_output;rs1_input", distances.get("C-CS1_outputC-RS1_input")));
        this.add(new Atom("distance", "cs1_output;rs1_output", distances.get("C-CS1_outputC-RS1_output")));
        this.add(new Atom("distance", "cs1_output;rs2_input", distances.get("C-CS1_outputC-RS2_input")));
        this.add(new Atom("distance", "cs1_output;rs2_output", distances.get("C-CS1_outputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "cs1_output;cs1_input", distances.get("C-CS1_outputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "cs1_output;cs2_input", distances.get("C-CS1_outputC-CS2_shelf")));

        this.add(new Atom("distance", "cs2_input;bs", distances.get("C-CS2_inputC-BS_output")));
        this.add(new Atom("distance", "cs2_input;ds", distances.get("C-CS2_inputC-DS_input")));
        this.add(new Atom("distance", "cs2_input;cs1_input", distances.get("C-CS2_inputC-CS1_input")));
        this.add(new Atom("distance", "cs2_input;cs1_output", distances.get("C-CS2_inputC-CS1_output")));
        this.add(new Atom("distance", "cs2_input;cs2_input", 0));
        this.add(new Atom("distance", "cs2_input;cs2_output", distances.get("C-CS2_inputC-CS2_output")));
        this.add(new Atom("distance", "cs2_input;rs1_input", distances.get("C-CS2_inputC-RS1_input")));
        this.add(new Atom("distance", "cs2_input;rs1_output", distances.get("C-CS2_inputC-RS1_output")));
        this.add(new Atom("distance", "cs2_input;rs2_input", distances.get("C-CS2_inputC-RS2_input")));
        this.add(new Atom("distance", "cs2_input;rs2_output", distances.get("C-CS2_inputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "cs2_input;cs1_input", distances.get("C-CS2_inputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "cs2_input;cs2_input", distances.get("C-CS2_inputC-CS2_shelf")));

        this.add(new Atom("distance", "cs2_output;bs", distances.get("C-CS2_outputC-BS_output")));
        this.add(new Atom("distance", "cs2_output;ds", distances.get("C-CS2_outputC-DS_input")));
        this.add(new Atom("distance", "cs2_output;cs1_input", distances.get("C-CS2_outputC-CS1_input")));
        this.add(new Atom("distance", "cs2_output;cs1_output", distances.get("C-CS2_outputC-CS1_output")));
        this.add(new Atom("distance", "cs2_output;cs2_input", distances.get("C-CS2_outputC-CS2_input")));
        this.add(new Atom("distance", "cs2_output;cs2_output", 0));
        this.add(new Atom("distance", "cs2_output;rs1_input", distances.get("C-CS2_outputC-RS1_input")));
        this.add(new Atom("distance", "cs2_output;rs1_output", distances.get("C-CS2_outputC-RS1_output")));
        this.add(new Atom("distance", "cs2_output;rs2_input", distances.get("C-CS2_outputC-RS2_input")));
        this.add(new Atom("distance", "cs2_output;rs2_output", distances.get("C-CS2_outputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "cs2_output;cs1_input", distances.get("C-CS2_outputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "cs2_output;cs2_input", distances.get("C-CS2_outputC-CS2_shelf")));

        this.add(new Atom("distance", "rs1_input;bs", distances.get("C-RS1_inputC-BS_output")));
        this.add(new Atom("distance", "rs1_input;ds", distances.get("C-RS1_inputC-DS_input")));
        this.add(new Atom("distance", "rs1_input;cs1_input", distances.get("C-RS1_inputC-CS1_input")));
        this.add(new Atom("distance", "rs1_input;cs1_output", distances.get("C-RS1_inputC-CS1_output")));
        this.add(new Atom("distance", "rs1_input;cs2_input", distances.get("C-RS1_inputC-CS2_input")));
        this.add(new Atom("distance", "rs1_input;cs2_output", distances.get("C-RS1_inputC-CS2_output")));
        this.add(new Atom("distance", "rs1_input;rs1_input", 0));
        this.add(new Atom("distance", "rs1_input;rs1_output", distances.get("C-RS1_inputC-RS1_output")));
        this.add(new Atom("distance", "rs1_input;rs2_input", distances.get("C-RS1_inputC-RS2_input")));
        this.add(new Atom("distance", "rs1_input;rs2_output", distances.get("C-RS1_inputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "rs1_input;cs1_input", distances.get("C-RS1_inputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "rs1_input;cs2_input", distances.get("C-RS1_inputC-CS2_shelf")));

        this.add(new Atom("distance", "rs1_output;bs", distances.get("C-RS1_outputC-BS_output")));
        this.add(new Atom("distance", "rs1_output;ds", distances.get("C-RS1_outputC-DS_input")));
        this.add(new Atom("distance", "rs1_output;cs1_input", distances.get("C-RS1_outputC-CS1_input")));
        this.add(new Atom("distance", "rs1_output;cs1_output", distances.get("C-RS1_outputC-CS1_output")));
        this.add(new Atom("distance", "rs1_output;cs2_input", distances.get("C-RS1_outputC-CS2_input")));
        this.add(new Atom("distance", "rs1_output;cs2_output", distances.get("C-RS1_outputC-CS2_output")));
        this.add(new Atom("distance", "rs1_output;rs1_input", distances.get("C-RS1_outputC-RS1_input")));
        this.add(new Atom("distance", "rs1_output;rs1_output", 0));
        this.add(new Atom("distance", "rs1_output;rs2_input", distances.get("C-RS1_outputC-RS2_input")));
        this.add(new Atom("distance", "rs1_output;rs2_output", distances.get("C-RS1_outputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "rs1_output;cs1_input", distances.get("C-RS1_outputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "rs1_output;cs2_input", distances.get("C-RS1_outputC-CS2_shelf")));

        this.add(new Atom("distance", "rs2_input;bs", distances.get("C-RS2_inputC-BS_output")));
        this.add(new Atom("distance", "rs2_input;ds", distances.get("C-RS2_inputC-DS_input")));
        this.add(new Atom("distance", "rs2_input;cs1_input", distances.get("C-RS2_inputC-CS1_input")));
        this.add(new Atom("distance", "rs2_input;cs1_output", distances.get("C-RS2_inputC-CS1_output")));
        this.add(new Atom("distance", "rs2_input;cs2_input", distances.get("C-RS2_inputC-CS2_input")));
        this.add(new Atom("distance", "rs2_input;cs2_output", distances.get("C-RS2_inputC-CS2_output")));
        this.add(new Atom("distance", "rs2_input;rs1_input", distances.get("C-RS2_inputC-RS1_input")));
        this.add(new Atom("distance", "rs2_input;rs1_output", distances.get("C-RS2_inputC-RS1_output")));
        this.add(new Atom("distance", "rs2_input;rs2_input", 0));
        this.add(new Atom("distance", "rs2_input;rs2_output", distances.get("C-RS2_inputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "rs2_input;cs1_input", distances.get("C-RS2_inputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "rs2_input;cs2_input", distances.get("C-RS2_inputC-CS2_shelf")));

        this.add(new Atom("distance", "rs2_output;bs", distances.get("C-RS2_outputC-BS_output")));
        this.add(new Atom("distance", "rs2_output;ds", distances.get("C-RS2_outputC-DS_input")));
        this.add(new Atom("distance", "rs2_output;cs1_input", distances.get("C-RS2_outputC-CS1_input")));
        this.add(new Atom("distance", "rs2_output;cs1_output", distances.get("C-RS2_outputC-CS1_output")));
        this.add(new Atom("distance", "rs2_output;cs2_input", distances.get("C-RS2_outputC-CS2_input")));
        this.add(new Atom("distance", "rs2_output;cs2_output", distances.get("C-RS2_outputC-CS2_output")));
        this.add(new Atom("distance", "rs2_output;rs1_input", distances.get("C-RS2_outputC-RS1_input")));
        this.add(new Atom("distance", "rs2_output;rs1_output", distances.get("C-RS2_outputC-RS1_output")));
        this.add(new Atom("distance", "rs2_output;rs2_input", distances.get("C-RS2_outputC-RS2_input")));
        this.add(new Atom("distance", "rs2_output;rs2_output", 0));
        this.add(new Atom("distanceBuffer", "rs2_output;cs1_input", distances.get("C-RS2_outputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "rs2_output;cs2_input", distances.get("C-RS2_outputC-CS2_shelf")));

        this.add(new Atom("distance", "ds;bs", distances.get("C-DS_inputC-BS_output")));
        this.add(new Atom("distance", "ds;ds", 0));
        this.add(new Atom("distance", "ds;cs1_input", distances.get("C-DS_inputC-CS1_input")));
        this.add(new Atom("distance", "ds;cs1_output", distances.get("C-DS_inputC-CS1_output")));
        this.add(new Atom("distance", "ds;cs2_input", distances.get("C-DS_inputC-CS2_input")));
        this.add(new Atom("distance", "ds;cs2_output", distances.get("C-DS_inputC-CS2_output")));
        this.add(new Atom("distance", "ds;rs1_input", distances.get("C-DS_inputC-RS1_input")));
        this.add(new Atom("distance", "ds;rs1_output", distances.get("C-DS_inputC-RS1_output")));
        this.add(new Atom("distance", "ds;rs2_input", distances.get("C-DS_inputC-RS2_input")));
        this.add(new Atom("distance", "ds;rs2_output", distances.get("C-DS_inputC-RS2_output")));
        this.add(new Atom("distanceBuffer", "ds;cs1_input", distances.get("C-DS_inputC-CS1_shelf")));
        this.add(new Atom("distanceBuffer", "ds;cs2_input", distances.get("C-DS_inputC-CS2_shelf")));
    }
    
    
    /*
     * Checks the current stage of production of an order
     * @param order the order to check
     * @return the state from 0 to 5, with 0 = no piece retrieved and 5 = cap mounted on c3 product
     */
    public int getOrderState(Order order) {
        for (int i=0; i<5; i++) {
            if (!this.atomDao.findByNameAndAttributes("stage_c0_" + i, order.getId() + "").isEmpty()) {
                return i;
            }
        }
        return 0;
    }

    /*
     * Deletes from the Knowledge Base every atom concerning every product
     */
    public void resetOrderState() {
        List<String> atomNamesToDelete = Arrays.asList(
            "stage_c0_0", 
            "stage_c0_1",
            "stage_c0_2",
            "stage_c0_3",
            "capcolor",
            "ring1color",
            "ring2color",
            "ring3color",
            "tocheck",
            "complexity0",
            "complexity1",
            "complexity2",
            "complexity3",
            "deliveryWindowStart");

        for (Atom atom : atomDao.findAll()) {
            if (atomNamesToDelete.contains(atom.getName())) {
                atomDao.delete(atom);
            }
        }
    }

    /*
     * @return the list of products that are currently being held by the robots
     * or that are loaded in a machine 
     */
    public List<Order> getProductsInProduction() {
        List <Atom> stationHolding = atomDao.findByName("station_holding");
        List <Atom> robotHolding = atomDao.findByName("holding");
        List <Order> productsInProduction = new ArrayList<>();
        
        if (!stationHolding.isEmpty()) {
            for (Atom a : stationHolding) {
                int productId = Integer.parseInt(a.getAttributes().split(";")[1].substring(1));
                productsInProduction.add(refboxClient.getOrderById(productId));
            }
        }

        if (!robotHolding.isEmpty()) {
            for (Atom a : robotHolding) {
                int productId = Integer.parseInt(a.getAttributes().split(";")[1].substring(1));
                productsInProduction.add(refboxClient.getOrderById(productId));
            }
        }
        
        return productsInProduction;
        
    }

}
